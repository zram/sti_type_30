﻿#include "asf.h"
#define UART_BUF_SIZE 300
extern volatile uint16_t index_uart,input_uart_length;
extern volatile uint8_t uart_have_byte,uart_silence;
extern volatile uint8_t uart_mass[UART_BUF_SIZE];
extern volatile uint8_t  f_have_UART;
extern volatile uint8_t f_uart_tx_compl;
extern volatile uint16_t uart_length;
extern uint8_t* pp_232_UART;



void usart_init(void);
void usart_transmit(uint8_t* out_mass_UART, uint16_t lenght);
void usart_timer(void);