﻿/*
 * Mbus.h
 *
 * Created: 09.06.2016 15:16:04
 *  Author: Илья
 */ 





void mbus_transsmit(uint8_t* mbus_out, uint16_t lenght);
void mbus_pack_timer(void);
void read_values(uint8_t ch);
uint8_t mbus_crc(uint8_t *data,uint8_t len);
void get_value(uint8_t *addr);
void mbus_init(uint16_t baud);
uint8_t mbus_handler(void);



#define POWER_MOD_BUS_DIR PORTD.DIR|=PIN4_bm
#define POWER_MOD_BUS_ON PORTD.OUT|=PIN4_bm
#define POWER_MOD_BUS_OFF PORTD.OUT&=~PIN4_bm
#define  SILENCE 100
#define  MBUS_BUF_SIZE 65

extern uint8_t mbus_addr1[7],mbus_addr2[7];
extern uint8_t EEMEM mbus_addr1_nvm[7],mbus_addr2_nvm[7];

extern volatile uint16_t mbus_response_time;
extern volatile uint8_t f_have_mbus;
extern volatile uint8_t input_mbus[MBUS_BUF_SIZE];

