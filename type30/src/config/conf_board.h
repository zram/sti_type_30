/**
 * \file
 *
 * \brief User board configuration template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H
#include <avr/eeprom.h>

  void timer_callback (void);
void pin_dirs(void);
void start_timer (uint16_t time);
void d_mesage(uint8_t code);
void add_to_error(void);


//�����
extern volatile uint8_t f_gsm_is_on,error_hw;
extern volatile uint8_t  f_button;
extern volatile uint8_t  f_lights;
extern volatile uint8_t f_rtc;
extern volatile uint8_t  f_debug;
extern uint8_t gsm_state,f_sent;
//�������
extern volatile uint16_t config_timer_sec;
extern volatile uint16_t ms_timer;
extern volatile uint16_t sec_timer;
extern volatile uint16_t gsm_timer;
extern volatile uint8_t discharge_timer;
extern volatile uint16_t cycle_timer_min;
extern volatile uint8_t turn_gsm_on_timer;
extern volatile uint32_t time_out_timer;
extern volatile uint16_t time_try_timer;

//����������
extern uint16_t temp_cal;
extern uint8_t volatile error_tries;


//������

extern uint8_t ind_err;
extern uint8_t EEMEM ind_err_nvm;
extern uint8_t EEMEM archive_err_nvm[160];
#endif // CONF_BOARD_H