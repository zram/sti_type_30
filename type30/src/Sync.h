#include "asf.h"
#include <avr/eeprom.h>
void sync(void);
uint16_t settings_update(uint8_t * from_pc); 
 
 
extern uint8_t EEMEM error_nvm,charcge_count_nvm;
extern uint8_t EEMEM hours_sync_nvm; 
extern uint32_t EEMEM sheduler_nvm;
extern uint32_t EEMEM hours_work_nvm;
extern uint16_t EEMEM send_times_nvm;
extern uint8_t EEMEM  charge_count_nvm;
extern uint16_t EEMEM reset_count_nvm; 
extern uint32_t EEMEM ch1_nvm,ch2_nvm;
 
#define inerrupt_marker 1
#define inerrupt_cycle 2 
#define inerrupt_button 6
#define inerrupt_error 7
#define inerrupt_service 8
#define inerrupt_archive  4
extern uint16_t calib_ch1_4,calib_ch1_8,calib_ch1_12,calib_ch1_16,calib_ch1_20;
extern uint16_t calib_ch2_4,calib_ch2_8,calib_ch2_12,calib_ch2_16,calib_ch2_20;

extern uint16_t EEMEM calib_ch1_4_nvm,calib_ch1_8_nvm,calib_ch1_12_nvm,calib_ch1_16_nvm,calib_ch1_20_nvm;
extern uint16_t EEMEM calib_ch2_4_nvm,calib_ch2_8_nvm,calib_ch2_12_nvm,calib_ch2_16_nvm,calib_ch2_20_nvm;


extern uint16_t const calib_ch1_4_default, calib_ch1_8_default,calib_ch1_12_default,calib_ch1_16_default,calib_ch1_20_default;
extern uint16_t const calib_ch2_4_default, calib_ch2_8_default,calib_ch2_12_default,calib_ch2_16_default,calib_ch2_20_default;
 
 extern  uint16_t EEMEM  mark1_up_nvm,mark1_down_nvm,mark2_up_nvm,mark2_down_nvm,cycle_min_nvm;
 extern  uint8_t EEMEM  f_archive_start_nvm,archive_period_nvm,archive_size_nvm,f_mark_nvm,mark_period_nvm,sensor_on_nvm;


volatile struct telemetry_frame_t {
	uint8_t start;
	uint8_t id_reg;
	uint8_t id[11];
 
	uint8_t ch1_reg;
	uint32_t ch1;
	uint8_t ch2_reg;
	uint32_t ch2;
	uint8_t temp_reg;
	int8_t temp;
	uint8_t time_sync_reg;
	uint8_t time_sync;
	uint8_t sheduler_reg;
	uint32_t sheduler;
	uint8_t external_volt_reg;
	uint8_t external_volt;
	uint8_t internal_volt_reg;
	uint8_t internal_volt; 
	uint8_t charge_count_reg;
	uint8_t charge_count;	
	uint8_t csq_reg;
	uint8_t csq;
	uint8_t reset_count_reg;
	uint16_t reset_count;
	uint8_t last_error_reg;
	uint8_t last_error;
	uint8_t device_type_reg;
	uint8_t device_type;
	uint8_t send_times_reg;
	uint16_t send_times;
	uint8_t hours_work_reg;
	uint32_t hours_work;
	
	uint8_t f_archive_start_reg;
	uint8_t f_archive_start;
	
	uint8_t archive_period_reg;
	uint8_t archive_period;
	
	uint8_t archive_size_reg;
	uint8_t archive_size;	

	uint8_t f_mark_reg;
	uint8_t f_mark;

	uint8_t mark1_up_reg;
	uint16_t mark1_up;

	uint8_t mark1_down_reg;
	uint16_t mark1_down;	
	
	uint8_t mark2_up_reg;
	uint16_t mark2_up;

	uint8_t mark2_down_reg;
	uint16_t mark2_down;
	
	uint8_t mark_period_reg;
	uint8_t mark_period;	
	
	uint8_t sensor_on_reg;
	uint8_t sensor_on;
	
	uint8_t cycle_min_reg;
	uint16_t cycle_min;	
		
	uint8_t press_sensor1_reg;
	uint16_t press_sensor1;

	uint8_t press_sensor2_reg;
	uint16_t press_sensor2;

	uint8_t interrup_type_reg;
	uint8_t interrup_type;
	
	uint8_t error_records_num_reg;
	uint8_t error_records_num;	
	
	uint8_t f_tech_pack_reg;
	uint8_t f_tech_pack;
	
	uint8_t f_vers_reg;
	uint8_t f_vers;
 
	uint8_t flow_rate_reg1;
	uint8_t flow_rate1[4];
	uint8_t flow_accum_reg1;
	uint8_t flow_accum1[4];
	
	uint8_t flow_rate_reg2;
	uint8_t flow_rate2[4];
	uint8_t flow_accum_reg2;
	uint8_t flow_accum2[4];
 
	uint8_t error_archive_start;
	uint8_t error_archive[160];
}t_frame ;



volatile extern uint8_t f_sync;


#define d_ID 100
#define d_BUTTON 1
#define d_CH1 2
#define d_CH2 3
#define d_ADC_INT 6
#define d_ADC_EXT 7
#define d_ADC_TEMP 8
#define d_GSM_POWER_ON 9
#define d_GSM_POWER_OFF 10
#define d_GSM_SIM 11
#define d_GSM_NET 12
#define d_GSM_CSQ 13
#define d_GSM_GPRS 14
#define d_GSM_SERVER 15
#define d_GSM_OK 16
#define d_GSM_ERROR 17
#define d_SLEEP 18
#define d_RTC 19
#define	  d_ERROR_TURN 20
#define	  d_ERROR_SIM   21
#define	  d_ERROR_REG  22
#define	  d_ERROR_GPRS 23
#define	  d_ERROR_SERVER 24