﻿/*
* Mbus.c
*
* Created: 08.06.2016 18:40:55
*  Author: Илья
*/



#include "asf.h"
#include "main.h"
#include "Sync.h"
#include "Mbus.h"
#include "string.h"

uint8_t get_values_water[20] = {0xFE,0xFE,0xFE,0xFE,0x68,0x10,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x03,0x90,0x1F,0x01,0x2D,0x16};
//uint8_t get_values_water[20] ={0xFE,0xFE,0xFE,0xFE,0x68,0x10,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0x03,0x03,0x81,0x0A,0x01,0xB0,0x16};

uint8_t mbus_addr1[7],mbus_addr2[7];
uint8_t EEMEM mbus_addr1_nvm[7],mbus_addr2_nvm[7];

uint8_t volatile f_have_mbus,mbus_state;
uint8_t mbus_silence;
volatile uint16_t index_mbus,mbus_response_time;
volatile uint8_t mbus_have_byte;
volatile uint8_t input_mbus[MBUS_BUF_SIZE];
volatile uint8_t mbus_length;
volatile uint8_t f_mbus_compl;
volatile int mbus_weight;
uint8_t* p_mbus;
uint8_t attemps =0;


//****************инициаллизация USART************************************
void mbus_init(uint16_t baud)
{
	mbus_state=1;
	sysclk_enable_module(SYSCLK_PORT_D,PR_USART0_bm);
	index_mbus = 0;
	mbus_have_byte = false;
	mbus_silence = 0;
	USARTD0.CTRLA = 0x14;//0x28; //средний уровень прерывания// 0x14; // прерывания RXD и TXD низкого уровня
	USARTD0.CTRLB = 0x18; // разрешаем приём и передачу
	USARTD0.CTRLC = 0x23;
	USARTD0.BAUDCTRLA = baud;
	USARTD0.BAUDCTRLB = baud>>8;
	f_mbus_compl = true;
	PORTD.DIR |= 0x08; // передатчик
	PORTD.PIN3CTRL |=0x40;
	PORTD.PIN2CTRL |=0x40;
	
	for (uint8_t i=0; i<MBUS_BUF_SIZE;i++)
	{
		input_mbus[i]=0;
	}
	
	for (uint8_t i=0; i<4;i++)
	{
		t_frame.flow_rate1[i]=0;
		t_frame.flow_accum1[i]=0;
		t_frame.flow_rate2[i]=0;
		t_frame.flow_accum2[i]=0;
		
	}
	POWER_MOD_BUS_DIR; POWER_MOD_BUS_ON;
}

//*************************************************************************
//************прерывание по приёму байта***********************************
ISR(USARTD0_RXC_vect)
{
	input_mbus[index_mbus] = USARTD0.DATA;
	index_mbus++;
	mbus_have_byte = true;
	mbus_silence = 0;
}




//************передача по USART********************************************
void mbus_transsmit(uint8_t* mbus_out, uint16_t lenght)
{
	f_mbus_compl = false;
	USARTD0.DATA = mbus_out[0]; //передаём первый байт
	p_mbus = mbus_out;
	mbus_weight = lenght;
}


//************прерывание по передаче байта*****************************
ISR(USARTD0_TXC_vect)
{
	mbus_weight--;
	p_mbus++;
	if(mbus_weight)
	{
		USARTD0.DATA = *p_mbus;
	}
	else {
		f_mbus_compl = true; //гасим разрешение на передачу

	}
}
//*************************************************************************





void mbus_pack_timer(void)
{
	if(mbus_have_byte){
		mbus_silence++;
		if(mbus_silence>=SILENCE)
		
		{
			mbus_silence=0;
			mbus_have_byte = false;
			f_have_mbus=true;
			mbus_length=index_mbus;
			index_mbus = 0;
		}
	}
}
//*************************************************************************

//*************************************************************************



uint8_t mbus_handler(void)
{
	switch(mbus_state)
	{
		case 1: get_value(&mbus_addr1);	break;
		case 2: read_values(1);	break;
		case 3:	get_value(&mbus_addr2);	break;
		case 4: read_values(2);	break;
		case 5: POWER_MOD_BUS_OFF; return 1; break;
	}
	return 0;
}






void get_value(uint8_t *addr)
{
	
	if ((addr[0]==0&addr[1]==0&addr[2]==0&addr[3]==0&addr[4]==0&addr[5]==0&addr[6]==0))
	{
		mbus_state+=2;
	}
	else {
		
		uint8_t checksum=0;
		memcpy(&get_values_water[6],addr,7);
		
		
		for (uint8_t i=4; i<18;i++)
		{
			checksum+=get_values_water[i];
		}
		get_values_water[18]=checksum;
		
		for (uint8_t i=0; i<MBUS_BUF_SIZE;i++)
		{
			input_mbus[i]=0;
		}
		

		mbus_transsmit( get_values_water,20);
		
		mbus_state++;
		mbus_response_time=0;
	}
}



void read_values(uint8_t ch)
{
	uint8_t *data,i=0;
	if ((f_have_mbus)&&(input_mbus[0]==0xFE)&&(input_mbus[mbus_length-1]==0x16))
	{
		f_have_mbus=0;
		
		while (input_mbus[i]==0xFE)
		{
			
			i++;
			data = &input_mbus[i];
		}
		if (mbus_crc(data,(mbus_length-i)) == input_mbus[mbus_length-2])
		{
			for (uint8_t i=0; i<4;i++)
			{
				if (ch==1)
				{
					t_frame.flow_rate1[i]=input_mbus[23+i];
					t_frame.flow_accum1[i]=input_mbus[18+i];
				}
				else if (ch==2)
				{
					t_frame.flow_rate2[i]=input_mbus[23+i];
					t_frame.flow_accum2[i]=input_mbus[18+i];
				}
			}
			if (ch==1)
			{
				t_frame.flow_rate1[i]=input_mbus[23+i];
				t_frame.flow_accum1[i]=input_mbus[18+i];
			}
			else if (ch==2)
			{
				t_frame.flow_rate2[i]=input_mbus[23+i];
				t_frame.flow_accum2[i]=input_mbus[18+i];
				
			}
			
			mbus_state++;
			
		}
	}
	else
	if (mbus_response_time>3000)
	{
		if ((attemps>9)||((t_frame.interrup_type ==inerrupt_button)&&(attemps>2)))
		{
			if (ch ==1) mbus_state=3;
			if (ch ==2) mbus_state =5;
			attemps=0;
		}
		else
		{
			if (ch ==1) mbus_state = 1;
			if (ch ==2) mbus_state =2;
			attemps++;
		}
	}
}



uint8_t mbus_crc(uint8_t *data,uint8_t len)
{
	uint8_t check_sum=0;
	for (uint8_t j=0; j<(len-2);j++)
	{
		check_sum+=data[j];
	}
	return check_sum;
}

