﻿#include "asf.h"
#include "main.h"
#include "base64_enc.h"
#include "sync.h"
#include "string.h"
#include "usart.h"
#include "archive.h"
#include <stdio.h>
uint8_t gsm_state,f_sent;

#define  TURN_ON_GSM_TIME 8
#define POWER_OFF_CASE 100 
#define GSM_DID_NOT_TURN 20
uint8_t base_64[560];
uint8_t http_post[800] ="POST /device HTTP/1.0\r\nHost: mobilecounter.ru\r\nContent-Type:application/x-www-form-urlencoded\r\nContent-Length: 136\r\n\r\nPackage=";

//uint8_t connect_to_ip[] ="AT+CIPSTART=\"TCP\",\"000.000.000.000\",\"00000\"\r\0";

volatile uint8_t turn_gsm_on_timer=0;
t_key encryption_key = {
	0x42, 0x71, 0x90, 0x11, 0xFF, 0xC1, 0x42, 0x26,
	0x1F, 0xD2, 0xA1, 0x3F, 0x05, 0x25, 0x12, 0x01
};


t_data encryption_data;
t_data encrypted_data;
t_data decrypted_data;



uint8_t * at_send(uint8_t * at_com, uint8_t * at_resp, uint32_t time_out, uint16_t time_try, uint16_t length);
void turn_gsm_off(void);
uint8_t get_csq(uint8_t * mass);
uint8_t* add_ip(void);
uint8_t * response(uint8_t * at_resp);
void encrypt_package(void);
void error_handler (uint8_t code);
void success(void);


void turn_gsm_on(void)
{
	if (turn_gsm_on_timer==0) turn_gsm_on_timer= sec_timer;
	if (GSM_IS_OFF)
	{		
 
		CHARGE_DIR; CHARGE_ON;
		GSM_POWER_ON;
		GSM_BUT_ON;
		
		led(RED);
		if ((sec_timer-turn_gsm_on_timer)>TURN_ON_GSM_TIME)
		{
			turn_gsm_on_timer=0;
			error_handler(20);
			gsm_timer=0;
		}
	}
	else if (GSM_IS_ON)
	{
		t_frame.send_times++;
		eeprom_write_word(&send_times_nvm,t_frame.send_times);
		f_gsm_is_on=1;
		gsm_state=0;
		turn_gsm_on_timer=0;
		time_out_timer=0;
		led(OFF);
		GSM_BUT_OFF;
	}

}

void turn_gsm_off(void)
{
	if (turn_gsm_on_timer==0) turn_gsm_on_timer= sec_timer;
	if (GSM_IS_ON)
	{
		GSM_BUT_ON;
		if  ((sec_timer-turn_gsm_on_timer)>TURN_ON_GSM_TIME)
		{
			GSM_POWER_OFF;
			gsm_timer=0;
			gsm_state=0;
			turn_gsm_on_timer=0;
		}
	}
	else if (GSM_IS_OFF)
	{
		GSM_BUT_OFF;
		gsm_timer=0;
		gsm_state=0;
		turn_gsm_on_timer=0;
 
	}

}

void gsm_send_telemetry(void)
{
	uint16_t up_l;
		
	switch (gsm_state)
	{
		case 0: d_mesage(d_GSM_POWER_ON); gsm_state = 1;t_frame.f_tech_pack=0; break;
		case 1: if (at_send("ATE0\r\0","OK",1000,200,0)) gsm_state++; break;
		case 2: if (at_send("AT+CPIN?\r\0","+CPIN: READY",3000,200,0)) {gsm_state++;d_mesage(d_GSM_SIM);} break;
		case 3: if (at_send("AT+CREG?\r\0","CREG: 0,1",20000,200,0))   {gsm_state++; d_mesage(d_GSM_NET);}; break;
		case 4: if (get_csq(at_send("AT+CSQ\r\0","+CSQ:",1000,200,0))) {gsm_state++;d_mesage(d_GSM_CSQ);} break;   //примем пакет от сервера и поиск CSQ?
		case 5: if (at_send("AT+CGATT?\r\0","+CGATT: 1",180000,200,0))  {gsm_state++;d_mesage(d_GSM_GPRS);} break;
		case 6: if (at_send("AT+CSTT=\"CMNET\"\r\0","OK",1000,200,0))   gsm_state++; break;
		case 7: if (at_send("AT+CIICR\r\0","OK",30000,0,0))				gsm_state++; break;
		case 8: if (at_send("AT+CIFSR\r\0",".",10000,200,0))			gsm_state++; break;
		case 9: if (at_send("AT+CIPSTART=\"TCP\",\"mobilecounter.ru\",\"8080\"\r","CONNECT OK",6000,3000,0)){ gsm_state++;d_mesage(d_GSM_SERVER);}  break;
		case 10: if(at_send("AT+CIPSEND\r\0",">",1000,200,0))			gsm_state++; break;
		case 11: encrypt_package();										gsm_state++; break;
		case 12: up_l= settings_update((at_send(http_post,"[[",10000,0,0))); if (up_l>12){t_frame.f_tech_pack=1; gsm_state=14;} else if(up_l){ gsm_state++;d_mesage(d_GSM_OK);} break;
		case 13: success(); 	break;	
		case 14: if (at_send("AT+CIPCLOSE\r\0","CLOSE",3000,0,0)) { gsm_state=9;} break; 			
		case POWER_OFF_CASE: turn_gsm_off();	 break;
	}	
}
//массив ат команды//ожидаемый ответ//время на выполнение стэйта//время между попытками
uint8_t * at_send(uint8_t * at_com, uint8_t * at_resp, uint32_t time_out, uint16_t time_try, uint16_t length)
{
	uint8_t * mas_p=0;
	if (f_sent==0)
	{
		if (length==0)
		length=strlen(at_com);
		
		usart_transmit(at_com,length);
		time_try_timer=0;
		f_sent=1;
	}
	
	else
	{
		mas_p=response(at_resp);
		if (mas_p)
		{
			f_sent=0;
			time_out_timer=0;
			return mas_p;			
		}
		
		else
		if ((time_try<time_try_timer)&&(time_try))
		{
			f_sent =0;			//еще попытка
		}
		
		if (time_out<time_out_timer)
		{
			error_handler(gsm_state);
		}
	}
	return 0;
}

uint8_t * response(uint8_t * at_resp)
{
	if (f_have_UART)
	{
		f_have_UART=0;
		return strstr(&uart_mass,at_resp);
	} else return NULL;
}

uint8_t get_csq(uint8_t * mass)
{
	if (mass)
	{
		if (mass[7]==',')
		{
			mass[7]=mass[6];
			mass[6]='0';
		}
		t_frame.csq=(mass[6]-48)*10; //сохраняем CSQ
		t_frame.csq+=(mass[7]-48);
		return 1;
	}
	return 0;
}


uint8_t* add_ip(void)
{
/*	uint8_t a=0; uint16_t port;
	if ((ip_address>>24)>=100) a=0; else if ((ip_address>>24)>=10) a=1; else a=2;
	sprintf(connect_to_ip+19+a,"%d",ip_address>>24); connect_to_ip[22]='.';
	if (((ip_address>>16)&0xFF)>=100) a=0; else if (((ip_address>>16)&0xFF)>=10) a=1; else a=2;
	sprintf(connect_to_ip+23+a,"%d",(ip_address>>16)&0xFF);connect_to_ip[26]='.';
	if (((ip_address>>8)&0xFF)>=100) a=0; else if (((ip_address>>8)&0xFF)>=10) a=1; else a=2;
	sprintf(connect_to_ip+27+a,"%d",(ip_address>>8)&0xFF);connect_to_ip[30]='.';
	if (((ip_address>>0)&0xFF)>=100) a=0; else if (((ip_address>>0)&0xFF)>=10) a=1; else a=2;
	sprintf(connect_to_ip+31+a,"%d",(ip_address>>0)&0xFF);connect_to_ip[34]='"';
	
	port=ip_port; a=5;
	while (port)
	{
		port/=10;
		a--;
	}
	sprintf(connect_to_ip+37+a,"%u",ip_port);
	connect_to_ip[42]='"';
	return connect_to_ip;*/
}

void encrypt_package(void)
{
	uint8_t *pp; 
	
 
		uint16_t aes_count;
		sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_AES);
		aes_software_reset();
		aes_configure(AES_ENCRYPT, AES_MANUAL, AES_XOR_OFF);
		
		if (t_frame.interrup_type==inerrupt_archive)
		{
			 pp = &t_archive.start;	
			if (sizeof(t_archive)%16)
			aes_count = sizeof(t_archive)/16 + 1 ;
			else aes_count = sizeof(t_archive)/16;
		}else 
		{
			 pp = &t_frame.start;		
		if (sizeof(t_frame)%16)
		aes_count = sizeof(t_frame)/16 + 1 ;
		else aes_count = sizeof(t_frame)/16;
		}
		
		for (uint8_t i=0; i<aes_count;i++)
		{
			for (uint8_t j = 0; j<16;j++)
			{
			encryption_data[j]=pp[16*i+j];
			}
			aes_set_key(encryption_key);
			aes_write_inputdata(encryption_data);
			aes_start();
			do
			{
				
			}
			while (aes_is_busy());
			aes_read_outputdata(encrypted_data);
			for (uint8_t j = 0; j<16;j++)
			{
				http_post[126+16*i+j]=encrypted_data[j];
			}
		}
		sysclk_disable_module(SYSCLK_PORT_GEN, SYSCLK_AES);
		
		
		base64enc(base_64,http_post+126,(aes_count*16));
		int a = strlen(base_64);
		http_post[111]=(a+8)/100+48;
		http_post[112]=((a+8)/10)%10+48;
		http_post[113]=(a+8)%10+48;
		
		for (uint16_t i=0;i<a;i++)
		{
			http_post[126+i]=base_64[i];
		}
		
		http_post[strlen(http_post)]=26;
 
}


void error_handler (uint8_t code)
{ 
	if (code==GSM_DID_NOT_TURN) 
	{
		t_frame.last_error = 20; 
		d_mesage(d_ERROR_TURN);
	}
	else if (code==2) {t_frame.last_error = 100; d_mesage(d_ERROR_SIM); }
	else if (code==3) {t_frame.last_error = 101; d_mesage(d_ERROR_REG);}
	else if (code==12){ t_frame.last_error = 10; d_mesage(d_ERROR_SERVER);}
	else { t_frame.last_error = code;	d_mesage(d_GSM_ERROR);}
	add_to_error();
	gsm_state=POWER_OFF_CASE;
	d_mesage(d_ERROR_TURN);		
}

void success(void)
{
	gsm_state = POWER_OFF_CASE;
	d_mesage(d_GSM_POWER_OFF);	
	t_frame.last_error = 0;
	error_tries=0;
	led(GREEN);	
	
	if (t_frame.interrup_type==inerrupt_archive)
	{
		erase_archive();
	}
	
	else{
	//очистка архива ошибок
	for (uint16_t i=0;i<ind_err;i++) {t_frame.error_archive[i]=0;}
	ind_err=0;	eeprom_update_byte(&ind_err_nvm,ind_err);
	t_frame.error_records_num = ind_err/8;
	eeprom_update_block(t_frame.error_archive,archive_err_nvm,160);	
	}
}

