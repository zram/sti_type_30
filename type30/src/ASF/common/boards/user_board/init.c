

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include <avr/eeprom.h>
#include "main.h"
#include "Sync.h"
#include "adc.h"
#include "usart.h"
 #include "Mbus.h"
#include "string.h"

#define  FIRMWARE_VER 2

void led(uint8_t color);
uint8_t SP_ReadCalibrationByte( uint8_t index );

extern uint8_t get_values_water[20];

//������
uint8_t ind_err;
uint8_t EEMEM ind_err_nvm;
uint8_t EEMEM archive_err_nvm[160];


//�����
volatile uint8_t f_gsm_is_on,error_hw;
volatile uint8_t f_button;
volatile uint8_t f_rtc;
volatile uint8_t f_debug;  
volatile uint8_t f_lights=1;

//�������
volatile uint16_t config_timer_sec = 120;
volatile uint16_t ms_timer=0;
volatile uint16_t sec_timer=0;
volatile uint16_t gsm_timer=0;
volatile uint8_t discharge_timer=0;
volatile uint16_t cycle_timer_min=0;


volatile uint32_t time_out_timer=0;
volatile uint16_t time_try_timer=0;

volatile uint32_t global_ms_timer=0;

//����������
uint16_t temp_cal;


void port_interrupt_init(void);
void read_mem(void);
void start_timer (uint16_t time);
void set_2mhz(void);
void init_globals(void);
 void inline led_net(void);
static inline void rtc_init ( void );

void board_init(void)
{
	sysclk_init();						
	pmic_init();						
	irq_initialize_vectors();		 
	cpu_irq_enable();
	port_interrupt_init();	
	pin_dirs();	
	//2400 �� 48
 
	sleep_set_mode(SLEEP_SMODE_PSAVE_gc);
	wdt_set_timeout_period(WDT_TIMEOUT_PERIOD_8KCLK);
 
	read_mem();
	init_globals();
	udc_start();
	start_timer(49); //1 �� �� 48���		
	mesure();
	
 
	
	
	while (config_timer_sec) // ���� ���� ����� �� ������������	
	{		
 
		if (f_sync) {f_sync=0; sync();}
		if (ms_timer==999)	{led(GREEN);}
		if (ms_timer==100)   led(OFF); 
		if (f_debug){if (ms_timer==400)  led(RED); if(ms_timer==500) led(OFF); }
		if (f_button) break;
	}
	
	if (!f_debug)
	{
		if (f_button==0)
		 {t_frame.reset_count++; eeprom_write_word(&reset_count_nvm,t_frame.reset_count);}
		set_2mhz();
		rtc_init();		
		start_timer(1); // 1 �� �� 2���		
		udc_stop();
		sysclk_disable_usb();
	}
	turn_gsm_on_timer=0;
	wdt_enable();
	wdt_reset();	
}


void port_interrupt_init(void)
{
 	
		// ���� ������
		PORT_BUT.PIN2CTRL |= 0x01; //����������� �������� ������
		PORT_BUT.INT0MASK |= 0x04; //
		PORT_BUT.INTCTRL |= 0x01; //MD ���������� �� �����

		//����� 1
		PORT_CH1.PIN2CTRL |= 0x01; //����������� �������� ������
		PORT_CH1.INT0MASK |= 0x04; //
		PORT_CH1.INTCTRL |= 0x01; //MD ���������� �� �����
		
		//����� 2
		PORT_CH2.PIN2CTRL |= 0x01; //����������� �������� ������
		PORT_CH2.INT0MASK |= 0x04; //
		PORT_CH2.INTCTRL |= 0x01; //MD ���������� �� �����
  
}	

void pin_dirs(void)
{
 
	GSM_BUT_DIR;
	GSM_POWER_DIR;
	GSM_PULL_ON_OFF_DOWN;
	GSM_SENSOR_DIR; 
	CHARGE_DIR; CHARGE_ON;
 
}

void read_mem(void)
{
	t_frame.id[0]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, LOTNUM0) );
	t_frame.id[1]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, LOTNUM1) );
	t_frame.id[2]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, LOTNUM2) );
	t_frame.id[3]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, LOTNUM3) );
	t_frame.id[4]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, LOTNUM4) );
	t_frame.id[5]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, LOTNUM5) );
	t_frame.id[6]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, WAFNUM) );
	t_frame.id[7]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, COORDX0) );
	t_frame.id[8]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, COORDX1) );
	t_frame.id[9]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, COORDY0) );
	t_frame.id[10]=SP_ReadCalibrationByte( offsetof(NVM_PROD_SIGNATURES_t, COORDY1) );
	
	t_frame.ch1=eeprom_read_dword(&ch1_nvm);
	t_frame.ch2=eeprom_read_dword(&ch2_nvm);
 
	t_frame.time_sync=eeprom_read_byte(&hours_sync_nvm);
	t_frame.sheduler = eeprom_read_dword(&sheduler_nvm);
	t_frame.hours_work = eeprom_read_dword(&hours_work_nvm);
	t_frame.send_times = eeprom_read_word(&send_times_nvm); 
	t_frame.charge_count=eeprom_read_byte(&charge_count_nvm);
 
		
	t_frame.reset_count= eeprom_read_word(&reset_count_nvm);		
	ind_err = eeprom_read_byte(&ind_err_nvm);
	eeprom_read_block(t_frame.error_archive,archive_err_nvm,160);
	t_frame.error_records_num = ind_err/8;	
	temp_cal = adc_get_calibration_data(ADC_CAL_TEMPSENSE);	
	
	eeprom_read_block(&mbus_addr1,&mbus_addr1_nvm,7);
	eeprom_read_block(&mbus_addr2,&mbus_addr2_nvm,7);
	
	t_frame.f_archive_start=eeprom_read_byte(&f_archive_start_nvm );
	t_frame.archive_period=eeprom_read_byte(&archive_period_nvm );
	t_frame.archive_size=eeprom_read_byte(&archive_size_nvm);
	t_frame.f_mark=eeprom_read_byte(&f_mark_nvm);
	t_frame.mark1_up=eeprom_read_word(&mark1_up_nvm);
	t_frame.mark1_down=eeprom_read_word(&mark1_down_nvm);
	t_frame.mark2_up=eeprom_read_word(&mark2_up_nvm);
	t_frame.mark2_down=eeprom_read_word(&mark2_down_nvm);
	t_frame.mark_period=eeprom_read_byte(&mark_period_nvm);
	t_frame.sensor_on=eeprom_read_byte(&sensor_on_nvm);
	t_frame.cycle_min=eeprom_read_word(&cycle_min_nvm);
	
}

static inline void rtc_init ( void ) 
{
	sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_RTC);	
	while( ( RTC_STATUS & 0x01 ) );
	RTC_PER =59; // Period reg. value. Must subtract 1, 'cause zero value counted.
	RTC_CTRL = RTC_PRESCALER_DIV1024_gc; // div by 1, so 1.024 kHz oscillator
	RTC_INTCTRL |=  RTC_OVFINTLVL_LO_gc; // Set Int. priority level to low in RTC. Must match what's in PMIC_CTRL
	/* Set internal 1.024 kHz oscillator as clock source for RTC and enable it. */
	CLK_RTCCTRL = CLK_RTCSRC_ULP_gc | CLK_RTCEN_bm;
}

void start_timer (uint16_t time)
{
	sysclk_enable_module(SYSCLK_PORT_C, PR_TC1_bm);	
	TCC1.PER = time; // ��������� ������������
	TCC1.CTRLA = 0x10;//
	TCC1.INTCTRLA = 0x01; // low ������� ���������� �
	TCC1.CTRLA = 0x00; 
	TCC1.CTRLA = 0x07; //clk/1024  � ��������� ������
	TCC1.CNT =0;
}

void set_2mhz(void)
{
	OSC.CTRL |= OSC_RC32MEN_bm | OSC_RC2MEN_bm | OSC_RC32KEN_bm;
	while((OSC.STATUS & OSC_RC32MRDY_bm)==0){} // wait until stable
	while((OSC.STATUS & OSC_RC2MRDY_bm)==0){} // wait until stable
	while((OSC.STATUS & OSC_RC32KRDY_bm)==0){} // wait until stable
	// enable DFLL (on both RC clocks)
	DFLLRC2M.CTRL = DFLL_ENABLE_bm ;
	// And now switch to the 2M as a clocksource.
	CCP = CCP_IOREG_gc;            // protected write follows
	CLK.CTRL = CLK_SCLKSEL_RC2M_gc;   // The System clock is now the 2Mhz internal RC
	DFLLRC2M.CTRL =0 ;	

}

//������
ISR(PORTA_INT0_vect)
{
	f_button = 1;
	f_lights=1;
}

ISR(CH1_INT)
{
	t_frame.ch1++;
	d_mesage (d_CH1);
}

ISR(CH2_INT)
{
	t_frame.ch2++;
	d_mesage(d_CH2);
}

 

ISR(RTC_OVF_vect)
{
	f_rtc=1;

}

//���������� �������
 ISR(TCC1_OVF_vect)
{
	usart_timer(); 
	mbus_pack_timer(); 
	ms_timer++;
	
	if (ms_timer>999)			//1 ��� 
	 {
		ms_timer=0;
		sec_timer++;
		if (config_timer_sec) config_timer_sec--;
		if (gsm_timer) gsm_timer--;
	 }
 
	 mbus_response_time++;
 
	global_ms_timer++;
	 time_try_timer++;
	 time_out_timer++;
	led_net();
	
}

uint8_t SP_ReadCalibrationByte( uint8_t index )
{
	uint8_t result;
	/* Load the NVM Command register to read the calibration row. */
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	result = pgm_read_byte(index);
	/* Clean up NVM Command register. */
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	return result;
}

void init_globals(void)
{ 
	t_frame.start=0xF0;
	t_frame.id_reg=0x01; 
 
	t_frame.ch1_reg=0x0A; 
	t_frame.ch2_reg=0x0B; 
 
	t_frame.temp_reg=0x0E; 
	t_frame.time_sync_reg=0x15; 
	t_frame.sheduler_reg=0x16; 
	t_frame.external_volt_reg=0x17; 
	t_frame.internal_volt_reg=0x18;
 
	t_frame.charge_count_reg=0x19; 
 
	t_frame.csq_reg=0x1A; 
	t_frame.reset_count_reg=0x1d; 
	t_frame.last_error_reg=0x1e; 
	t_frame.device_type_reg=0x1F;
	t_frame.device_type=DEVICE_TYPE; 
	t_frame.send_times_reg=0x22; 
	t_frame.hours_work_reg=0x23; 
		 
		 
	t_frame.f_archive_start_reg=0x24;
	t_frame.archive_period_reg=0x25;
	t_frame.archive_size_reg=0x26;
	t_frame.f_mark_reg=0x2B;
	t_frame.mark1_up_reg=0x2C;
	t_frame.mark1_down_reg=0x2D;
	t_frame.mark2_up_reg=0x2E;
	t_frame.mark2_down_reg=0x2F;
	t_frame.mark_period_reg=0x34;
	t_frame.sensor_on_reg=0x36;
	t_frame.cycle_min_reg=0x3B;
	t_frame.press_sensor1_reg=0x3C;
	t_frame.press_sensor2_reg=0x3D;
		 
	t_frame.interrup_type_reg=0x37; 
	t_frame.error_records_num_reg=0x43; 
	t_frame.f_tech_pack_reg=0x45; 
	t_frame.f_vers_reg=89;
	t_frame.f_vers=FIRMWARE_VER;
	 
	t_frame.flow_rate_reg1=109; 
	t_frame.flow_accum_reg1=111;
	t_frame.flow_rate_reg2=110;
	t_frame.flow_accum_reg2=112;  
 
	t_frame.error_archive_start=0xF3; 

	t_archive.start = 0xF0;
	t_archive.id_reg =0x01;   
	strncpy(&t_archive.id,&t_frame.id,11);  
	t_archive.archive_size_reg=0x26;
	t_archive.archive_reg=0x27;
}

void led(uint8_t color)
{
	if (f_lights)
	{
		if (color==GREEN)
		{
			LED_RED_OFF;
			LED_GREEN_ON;
		}
		if (color==RED)
		{
			LED_RED_ON;
			LED_GREEN_OFF;
		}
		if (color==OFF)
		{
			LED_RED_OFF;
			LED_GREEN_OFF;
		}
	}
}

 void inline led_net(void)
{	
		if (f_lights)
		{
			
		
	if ((gsm_state>1)&&(gsm_state<4)&&(!(ms_timer%25)))
	{
		if (LED_RED_IS_ON) LED_RED_OFF; else LED_RED_ON;
	}
	
	
	if ((gsm_state==5)&&(!(ms_timer%50)))
	{
		if (LED_RED_IS_ON) LED_RED_OFF; else LED_RED_ON;
	}
		
		if (gsm_state==6)
		{
			led(RED);
		}	
		if (gsm_state==100) {LED_RED_OFF;}
		}
}