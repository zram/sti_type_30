
#include <asf.h>
#include "main.h"
#include "Sync.h"
#include "usart.h"
#include "Mbus.h"

static bool leap_test(struct calendar_date parm);
const char fusedata[] __attribute__ ((section (".fuse"))) =
//{0x00, 0x00, 0xFD, 0x00, 0xFE, 0xDC};
{0x00, 0x00, 0xFD, 0x00, 0xFE, 0xDE}; 
void wake_up(void);
void rtc_timer(void);
void button_handler(void);
void enter_sleep_mode(void);
uint8_t f_chardge_start=1,f_mesured,f_mesured_pres;
uint8_t volatile error_tries;
uint8_t mark_timer=0;
uint8_t min_hour_timer=0,archive_timer;
volatile struct calendar_date date = {
	.hour = 17,
	.date = 27,
	.month = 2,
	.year = 2017
};

const volatile uint8_t monthss[2][12] =
{
	{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
	{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
};

 

void encrypt_package(void);
 
int main (void)
{
 
	board_init();
 
	while(1)
	{
		
		
		wdt_reset();
		if (f_rtc) {f_rtc=0; rtc_timer();}
		if (f_button) button_handler();

		while(gsm_timer)
		{
			wdt_reset();
			if (!f_mesured) mesure(); else 
				if (!f_mesured_pres) mesure_presure(0); else
 
	
 	
			if (f_gsm_is_on)						 // ���� gsm ������ ������� - ��������
			{	
				gsm_send_telemetry();
			}
			else
			{
				if (mbus_handler()==1)	
				{
					LED_BLUE_OFF;
					turn_gsm_on();
				}
				else LED_BLUE_ON;
			}									 // �������� ����� �� �������
		}
		enter_sleep_mode();
	}
}


void rtc_timer(void)
{
	d_mesage(d_RTC);
	sleep_disable();
	start_timer(1);
	wdt_enable();
	wdt_reset();
	calendar_add_minute_to_date(&date);

	if (t_frame.cycle_min)
	{
		cycle_timer_min++;
	}
	if ((cycle_timer_min>=t_frame.cycle_min)&&(t_frame.cycle_min))
	{
		cycle_timer_min=0;
		t_frame.interrup_type=inerrupt_cycle;
		error_tries=0;
		wake_up();
	}
	
	if ((t_frame.mark_period)&&(t_frame.f_mark)) mark_timer++;
	
	if ((mark_timer>=t_frame.mark_period)&&(t_frame.f_mark))
	{
		mark_timer=0;		 
		mesure_presure(0);
		if ((t_frame.press_sensor1<t_frame.mark1_down)&&(t_frame.mark1_down)) {t_frame.interrup_type=inerrupt_marker; wake_up();}
		if ((t_frame.press_sensor1>t_frame.mark1_up)&&(t_frame.mark1_up)) {t_frame.interrup_type=inerrupt_marker; wake_up();}
		if ((t_frame.press_sensor2<t_frame.mark2_down)&&(t_frame.mark2_down)) {t_frame.interrup_type=inerrupt_marker; wake_up();}
		if ((t_frame.press_sensor2>t_frame.mark2_up)&&(t_frame.mark2_up)) {t_frame.interrup_type=inerrupt_marker; wake_up();}			
	}
	

	
	//���
	min_hour_timer++;
	if (min_hour_timer>59)
	{
		min_hour_timer=0;
		t_frame.hours_work++;
		eeprom_update_dword(&hours_work_nvm,t_frame.hours_work);		
		eeprom_update_dword(&ch1_nvm,t_frame.ch1);
		eeprom_update_dword(&ch2_nvm,t_frame.ch2);
		eeprom_busy_wait();
		if (((t_frame.sheduler>>date.date)&0x01)&&(!(t_frame.cycle_min))&&(date.hour==(t_frame.time_sync))) //if date is the same
		{ t_frame.interrup_type=inerrupt_cycle;wake_up();}
	
		if (((date.date+1)==monthss[leap_test(date)][date.month])&&((t_frame.sheduler>>(date.date+1)&0x01)||(t_frame.sheduler>>(date.date+1)&0x02)||(t_frame.sheduler>>(date.date+1)&0x04))&&(date.hour==t_frame.time_sync))
		{t_frame.interrup_type=inerrupt_cycle;wake_up();}
	
 
		
		if (t_frame.f_archive_start)
		{
			archive_timer++;
			if (get_archive_size()>= t_frame.archive_size)
				{
					t_frame.interrup_type=inerrupt_archive;
					wake_up();
				}
				else if (archive_timer>=t_frame.archive_period)
				{
					archive_timer=0;
					mesure_presure(0);
					add_to_archive();
				}
			
		}	
		
		if ((t_frame.last_error)&&(error_tries<3))
		{
			t_frame.interrup_type=inerrupt_error;
			wake_up();
			error_tries++;
		}
		
		if ((date.hour==10) && (error_tries>=3) && (t_frame.last_error))
		{
			t_frame.interrup_type=inerrupt_error;
			wake_up();
			error_tries++;
		}
		
		if (( date.date==24)&&(date.hour==5)) //if date is the same
		{wake_up(); t_frame.interrup_type=inerrupt_service;}
	
	
		discharge_timer++;
		if (discharge_timer>=PERIOD_BAT_TEST_h)
		{
			discharge_timer=0;		
			mesure();
		}
	}
}

void button_handler(void)
{
	if (gsm_timer==0)
	{	
		d_mesage(d_BUTTON);
		f_button=0;
		t_frame.interrup_type =inerrupt_button;
		wake_up();
	}
}

void enter_sleep_mode(void)
{
	f_lights=0;
	sec_timer=0;
	d_mesage(d_SLEEP);
	if (f_debug)
	{
		reset_do_soft_reset ();
	}
	PR.PRGEN =0x53;
	PR.PRPA = 0x07;
	PR.PRPB = 0x07;
	PR.PRPC = 0x7F;//0x5E;
	PR.PRPD = 0x7F;//0x5F;
	PR.PRPE = 0x7F;
	PR.PRPF = 0x7F;
	
	PORTA.DIR = 0x00;	PORTA.OUT = 0x00;
	PORTB.DIR = 0x00;	PORTB.OUT = 0x00;
	PORTC.DIR = 0x00;	PORTC.OUT = 0x00;
	PORTD.DIR = 0x00;	PORTD.OUT = 0x00;
	PORTE.DIR = 0x00;	PORTE.OUT = 0x00;
	 
	
 
	if (f_chardge_start){PORTR.DIR &= 0x01;	PORTR.OUT &= 0x01;}
	else {PORTR.DIR = 0x01;	PORTR.OUT = 0x00;}
 
	
	wdt_disable();
	sleep_enable();
	sleep_enter();
	
}



 


static bool leap_test(struct calendar_date parm)
{
	if(!((parm.year) % 4) && (((parm.year) % 100) || !((parm.year) % 400))) {
		return true;
		} else {
		return false;
	}
}

void wake_up(void)
{
	uint32_t delay_timer = global_ms_timer;
	if(!f_debug)
	{	
		sleep_disable();
		wdt_enable();
		wdt_reset();
		start_timer(1);
		pin_dirs(); 
		mbus_init(51); 
		while (delay_timer+1000>global_ms_timer) {}
		pmic_init();
		
	}
	usart_init();
	gsm_timer=GPRS_TIMEOUT;
	f_mesured=0;
	f_mesured_pres=0;
	turn_gsm_on_timer=0;
	f_gsm_is_on=0;
}


void add_to_error(void)
{
	if (t_frame.last_error ||error_hw){
		volatile uint8_t mas_n=0;
		
		t_frame.error_archive[ind_err+mas_n]=date.hour; mas_n++;
		t_frame.error_archive[ind_err+mas_n]=date.date; mas_n++;
		t_frame.error_archive[ind_err+mas_n]=date.month; mas_n++;
		t_frame.error_archive[ind_err+mas_n]=date.year%100; mas_n++;

		t_frame.error_archive[ind_err+mas_n]=t_frame.last_error;mas_n++;
		t_frame.error_archive[ind_err+mas_n]=t_frame.csq;mas_n++;
		t_frame.error_archive[ind_err+mas_n]=error_hw;mas_n++;
		t_frame.error_archive[ind_err+mas_n]=t_frame.temp;	mas_n++;
		
		eeprom_write_block(&t_frame.error_archive[ind_err],&archive_err_nvm[ind_err],8);
		ind_err+=8;
		t_frame.error_records_num = ind_err/8;
		if (ind_err>=160) {ind_err =0;}
	
	eeprom_write_byte(&ind_err_nvm,ind_err);}
}