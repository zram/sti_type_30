/*
 * archive.h
 *
 * Created: 24.10.2017 15:35:44
 *  Author: laptop
 */ 


#ifndef ARCHIVE_H_
#define ARCHIVE_H_

#define  MAX_ARCHIVE_SIZE 12
uint8_t get_archive_size(void);
void add_archive(void);
void add_to_archive(void);
void erase_archive(void);

  struct archive_m
{
	uint32_t timestamp;
	uint32_t ch1;
	uint32_t ch2;
	uint16_t press_sensor1;
	uint16_t press_sensor2;
	uint8_t flow_rate1[4];
	uint8_t flow_accum1[4];
	uint8_t flow_rate2[4]; 
	uint8_t flow_accum2[4];	
};

volatile struct archive_frame_t {
	uint8_t start;
	uint8_t id_reg;
	uint8_t id[11];
	uint8_t archive_size_reg;
	uint8_t archive_size;
	uint8_t archive_reg;
	struct archive_m archive_mass[MAX_ARCHIVE_SIZE];
}t_archive;

#endif /* ARCHIVE_H_ */