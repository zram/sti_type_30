﻿#include "asf.h"
#include "main.h"
#include "Sync.h"
 
void adc_init(void );
void charging(void);
int8_t get_temp(float temp);

volatile float ch1_k_400,ch1_k_800,ch1_k_1200,ch1_k_1600;
volatile float ch1_b_400,ch1_b_800,ch1_b_1200,ch1_b_1600;
volatile float ch2_k_400,ch2_k_800,ch2_k_1200,ch2_k_1600;
volatile float ch2_b_400,ch2_b_800,ch2_b_1200,ch2_b_1600;


void adc_init(void )
{
	 
	sysclk_enable_module(SYSCLK_PORT_A, SYSCLK_ADC);
	adc_enable(&ADCA);
	
	struct adc_config adc_conf;
	struct adc_channel_config adcch_conf;

	adc_read_configuration(&ADCA, &adc_conf);
	adcch_read_configuration(&ADCA, ADC_CH0, &adcch_conf);

	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_BANDGAP);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_conf, 100000UL);
  
	
	adc_enable_internal_input(&adc_conf, ADC_INT_TEMPSENSE); //вроде как времено -- терморезюк убрали
	
	adcch_set_input(&adcch_conf, VOLTAGE_BAT, ADCCH_NEG_NONE, 1);  // Напряжение батареи
	adcch_write_configuration(&ADCA, ADC_CH0, &adcch_conf);

	adcch_set_input(&adcch_conf, VOLTAGE_INTERNAL, ADCCH_NEG_NONE, 1);  // Напряжение внешнее
	adcch_write_configuration(&ADCA, ADC_CH1, &adcch_conf);
 
	adcch_set_input(&adcch_conf, TEMPERATURE, ADCCH_NEG_NONE, 1); //Температура внутреннее
	adcch_write_configuration(&ADCA, ADC_CH2, &adcch_conf);
  
  	adcch_set_input(&adcch_conf, ZERO_ADC, ADCCH_NEG_NONE, 1); // Замер нуля
  	adcch_write_configuration(&ADCA, ADC_CH3, &adcch_conf);
  
	adc_write_configuration(&ADCA, &adc_conf);
}
 
void adc_init_pressure(void )
{
	 
	sysclk_enable_module(SYSCLK_PORT_A, SYSCLK_ADC);
	adc_enable(&ADCA);
	GSM_SENSOR_ON;	
	struct adc_config adc_conf;
	struct adc_channel_config adcch_conf;

	adc_read_configuration(&ADCA, &adc_conf);
	adcch_read_configuration(&ADCA, ADC_CH0, &adcch_conf);

	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_BANDGAP);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_conf, 100000UL);
	
	
	adc_enable_internal_input(&adc_conf, ADC_INT_TEMPSENSE); //вроде как времено -- терморезюк убрали
	
	adcch_set_input(&adcch_conf, PRESS_SENSOR_1, ADCCH_NEG_NONE, 1);  // Датчик 1
	adcch_write_configuration(&ADCA, ADC_CH0, &adcch_conf);

	adcch_set_input(&adcch_conf, PRESS_SENSOR_2, ADCCH_NEG_NONE, 1);  // Датчик 2
	adcch_write_configuration(&ADCA, ADC_CH1, &adcch_conf);
 
   	adcch_set_input(&adcch_conf, ZERO_ADC, ADCCH_NEG_NONE, 1); // замер нуля
   	adcch_write_configuration(&ADCA, ADC_CH2, &adcch_conf);
	
	adc_write_configuration(&ADCA, &adc_conf);
}

void mesure (void)
{
	volatile int32_t adc_value=0;
	volatile float temperat=0; 
	int8_t zero_adc;
	
	adc_init(); 

	adc_start_conversion(&ADCA, ADC_CH0 | ADC_CH1 | ADC_CH2   );
	adc_wait_for_interrupt_flag(&ADCA, ADC_CH0 | ADC_CH1 | ADC_CH2 );		
 
	zero_adc=(int8_t)adc_get_result(&ADCA, ADC_CH3);
 
	temperat =	adc_get_result(&ADCA, ADC_CH2);
	temperat=((temperat*2+160)*358/(temp_cal))-275;
	t_frame.temp=(int8_t)temperat;
	
	
	adc_value=adc_get_result(&ADCA, ADC_CH1)-zero_adc;
	adc_value=adc_value*1550/2048;
	t_frame.external_volt=adc_value/10;
	
	adc_value=adc_get_result(&ADCA, ADC_CH0)-zero_adc;   //батарея
	adc_value=adc_value*555/2048;
	t_frame.internal_volt=adc_value/10;	
	
	if ((adc_value<376)&&(t_frame.external_volt>45))
	{
		CHARGE_DIR;
		CHARGE_ON;
		f_chardge_start=1;
	}
	else if (adc_value>400)
	{
		CHARGE_OFF;
		if (f_chardge_start==1) // 
		{
			f_chardge_start=0;
			t_frame.charge_count++;
			eeprom_update_byte(&charcge_count_nvm, t_frame.charge_count);
		}
	} else if (t_frame.external_volt<45) CHARGE_OFF; 
	
	f_mesured =1;
	
	
}

void mesure_presure (uint8_t f_calib)
{
	if (t_frame.sensor_on)
	{
	float x1_float=0, x2_float=0;	
	int8_t zero_adc;
	volatile int32_t adc_value=0; 
	uint32_t delay_timer=global_ms_timer;
	
	adc_init_pressure();
	
	while (delay_timer+3000>global_ms_timer) {}

	adc_start_conversion(&ADCA, ADC_CH0 | ADC_CH1 | ADC_CH2   );
	adc_wait_for_interrupt_flag(&ADCA, ADC_CH0 | ADC_CH1 | ADC_CH2 );
	
	zero_adc=(int8_t)adc_get_result(&ADCA, ADC_CH2);
	 
	
	adc_value=(int16_t)adc_get_result(&ADCA, ADC_CH0)-zero_adc;
	adc_value=adc_value*2000/2048;
	if ((adc_value<0)||(adc_value<15))adc_value=0;
	t_frame.press_sensor1=adc_value;
	
	adc_value=(int16_t)adc_get_result(&ADCA, ADC_CH1)-zero_adc;   
	adc_value=adc_value*2000/2048;
	if ((adc_value<0)||(adc_value<15))adc_value=0;
	t_frame.press_sensor2=adc_value;
	
 
	/*	if (!f_calib)		
		{   x1_float=t_frame.press_sensor1; x2_float=t_frame.press_sensor2;
			if (t_frame.press_sensor1<=800) {t_frame.press_sensor1=ch1_k_400*x1_float + ch1_b_400;}
			if (t_frame.press_sensor1>800 && t_frame.press_sensor1<=1200){t_frame.press_sensor1=ch1_k_800*x1_float + ch1_b_800;}
			if (t_frame.press_sensor1>1200 && t_frame.press_sensor1<=1600){t_frame.press_sensor1=ch1_k_1200*x1_float + ch1_b_1200;}
			if (t_frame.press_sensor1>1600 && t_frame.press_sensor1<=2100){t_frame.press_sensor1=ch1_k_1600*x1_float + ch1_b_1600;}
			
			if (t_frame.press_sensor2<=800) {t_frame.press_sensor2=ch2_k_400*x2_float + ch2_b_400;}
			if (t_frame.press_sensor2>800 && t_frame.press_sensor2<=1200){t_frame.press_sensor2=ch2_k_800*x2_float + ch2_b_800;}
			if (t_frame.press_sensor2>1200 && t_frame.press_sensor2<=1600){t_frame.press_sensor2=ch2_k_1200*x2_float + ch2_b_1200;}
			if (t_frame.press_sensor2>1600 && t_frame.press_sensor2<=2100){t_frame.press_sensor2=ch2_k_1600*x2_float + ch2_b_1600;}
		}*/
	
	}
	 f_mesured_pres =1; 
	 GSM_SENSOR_OFF;
}
 