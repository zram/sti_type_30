#include <asf.h>
#include "archive.h"
#include "Sync.h"


extern struct calendar_date date;
void add_to_archive(void)
{
	
	if (t_archive.archive_size<MAX_ARCHIVE_SIZE) 
	{
		
	
	t_archive.archive_mass[t_archive.archive_size].timestamp = calendar_date_to_timestamp(&date);
	t_archive.archive_mass[t_archive.archive_size].ch1= t_frame.ch1;
	t_archive.archive_mass[t_archive.archive_size].ch2= t_frame.ch2;
	t_archive.archive_mass[t_archive.archive_size].press_sensor1= t_frame.press_sensor1;
	t_archive.archive_mass[t_archive.archive_size].press_sensor2= t_frame.press_sensor2;
	
	t_archive.archive_mass[t_archive.archive_size].flow_rate1[0]= t_frame.flow_rate1[0];
	t_archive.archive_mass[t_archive.archive_size].flow_rate1[1]= t_frame.flow_rate1[1];
	t_archive.archive_mass[t_archive.archive_size].flow_rate1[2]= t_frame.flow_rate1[2];
	t_archive.archive_mass[t_archive.archive_size].flow_rate1[3]= t_frame.flow_rate1[3];
	t_archive.archive_mass[t_archive.archive_size].flow_accum1[0]= t_frame.flow_accum1[0];
	t_archive.archive_mass[t_archive.archive_size].flow_accum1[1]= t_frame.flow_accum1[1];	
	t_archive.archive_mass[t_archive.archive_size].flow_accum1[2]= t_frame.flow_accum1[2];	
	t_archive.archive_mass[t_archive.archive_size].flow_accum1[3]= t_frame.flow_accum1[3];	
	
	t_archive.archive_mass[t_archive.archive_size].flow_rate2[0]= t_frame.flow_rate2[0];
	t_archive.archive_mass[t_archive.archive_size].flow_rate2[1]= t_frame.flow_rate2[1];
	t_archive.archive_mass[t_archive.archive_size].flow_rate2[2]= t_frame.flow_rate2[2];
	t_archive.archive_mass[t_archive.archive_size].flow_rate2[3]= t_frame.flow_rate2[3];
	t_archive.archive_mass[t_archive.archive_size].flow_accum2[0]= t_frame.flow_accum2[0];
	t_archive.archive_mass[t_archive.archive_size].flow_accum2[1]= t_frame.flow_accum2[1];
	t_archive.archive_mass[t_archive.archive_size].flow_accum2[2]= t_frame.flow_accum2[2];
	t_archive.archive_mass[t_archive.archive_size].flow_accum2[3]= t_frame.flow_accum2[3];
				
	t_archive.archive_size++;
	}
}

uint8_t get_archive_size(void)
{
	return t_archive.archive_size;
}

void erase_archive(void)
{
	t_archive.archive_size=0;
}