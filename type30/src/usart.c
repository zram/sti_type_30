﻿
#include "asf.h"
#include "usart.h"
#include "main.h"

#define TIME_DELAY_UART 5

volatile uint16_t index_uart,input_uart_length;
volatile uint8_t uart_have_byte,uart_silence;
volatile uint8_t uart_mass[UART_BUF_SIZE];  //макс конфиг пакет около 80 байт
volatile uint8_t f_have_UART;
volatile uint8_t f_uart_tx_compl; 
volatile uint16_t uart_length; 
uint8_t* pp_232_UART ;



void usart_init(void)
{  
	sysclk_enable_module(GSM_UART_PORT,GSM_UART);
   uart_have_byte = false;
   uart_silence = 0;
   USART.CTRLA = 0x14;
   USART.CTRLB = 0x18; 
   if(f_debug) USART.BAUDCTRLA = 51;
   else
   USART.BAUDCTRLA = 12;	//12 - 9600 на 2 мгц 
   USART.BAUDCTRLB = 0;
   f_uart_tx_compl = true;
   UART_TX_DIR;
}

 
ISR(USART_RXC_vect)
{ 
	if (index_uart>UART_BUF_SIZE)index_uart=0; 		 
	 uart_mass[index_uart] = USART.DATA;
	 index_uart++;
	 uart_have_byte = true;
	 uart_silence = 0;  
}



//************�������� �� USART********************************************
void usart_transmit(uint8_t* out_mass_UART, uint16_t lenght)
{ 
	for (uint16_t i=0; i<UART_BUF_SIZE; i++)
	{
		uart_mass[i]=0;
	}
	
	if (f_uart_tx_compl)
	{
		if (lenght>0)
		{
			f_uart_tx_compl = false;
			const uint8_t *litera;
			litera = out_mass_UART; 
			USART.DATA = *litera; 
			pp_232_UART = (uint8_t*)litera;
			uart_length = lenght;
		}
	 }
}
//************��������� �������� �� USART*********************************

//************���������� �� �������� �����*****************************
ISR(USART_TXC_vect)
{
  uart_length--;
  pp_232_UART++;
  if(uart_length){        
   USART.DATA = *pp_232_UART;
  }
  else {
	  f_uart_tx_compl = true;  
  }
 }
//*************************************************************************



 void usart_timer(void)
{
  if(uart_have_byte){
  uart_silence++;
  if(uart_silence>=TIME_DELAY_UART){

                       uart_silence=0;	
                       uart_have_byte = false;                     
					   input_uart_length=index_uart;
                       index_uart = 0;
					   f_have_UART=1;
  }
  }
}
