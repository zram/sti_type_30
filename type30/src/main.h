﻿
#include "archive.h"

void led(uint8_t color);
void mesure (void);
extern uint8_t f_chardge_start,f_mesured,f_mesured_pres;
void gsm_send_telemetry(void);
void adc_init(void );
void adc_init_pressure(void );
void mesure_presure (uint8_t f_calib);

extern volatile uint32_t global_ms_timer;

extern volatile float ch1_k_400,ch1_k_800,ch1_k_1200,ch1_k_1600;
extern volatile float ch1_b_400,ch1_b_800,ch1_b_1200,ch1_b_1600;
extern volatile float ch2_k_400,ch2_k_800,ch2_k_1200,ch2_k_1600;
extern  volatile float ch2_b_400,ch2_b_800,ch2_b_1200,ch2_b_1600;

void turn_gsm_on(void);
#define  GREEN 1
#define  RED 2
#define  OFF 3

#define PERIOD_BAT_TEST_h 2
#define GPRS_TIMEOUT 300


 
 
 
 

 #define  DEVICE_TYPE 30
#define  BUTTON_PRESSED PORTA.IN&PIN2_bm
#define GSM_UART_PORT SYSCLK_PORT_C
#define GSM_UART PR_USART0_bm

#define  USART	USARTC0

#define  USART_RXC_vect USARTC0_RXC_vect
#define  USART_TXC_vect USARTC0_TXC_vect

#define LED_GREEN_DIR	PORTC.DIR|=PIN0_bm
#define LED_RED_DIR		PORTC.DIR|=PIN1_bm
#define LED_BLUE_DIR		PORTC.DIR|=PIN1_bm

#define LED_BLUE_ON		PORTB.DIR|=PIN1_bm
#define LED_BLUE_OFF	PORTB.DIR&=~PIN1_bm

#define LED_GREEN_ON	PORTC.DIR|=PIN0_bm
#define LED_GREEN_OFF	PORTC.DIR&=~PIN0_bm

#define LED_RED_ON		PORTC.DIR|=PIN1_bm
#define LED_RED_OFF		PORTC.DIR&=~PIN1_bm

#define LED_GREEN_IS_ON PORTC.DIR&PIN0_bm
#define LED_RED_IS_ON	PORTC.DIR&PIN1_bm

#define UART_TX_DIR		PORTC.DIR|=PIN3_bm

#define GSM_BUT_DIR		PORTD.DIR|=PIN0_bm
#define GSM_BUT_ON		PORTD.OUT|=PIN0_bm
#define GSM_BUT_OFF		PORTD.OUT&=~PIN0_bm

#define GSM_POWER_DIR	PORTE.DIR|=PIN0_bm
#define GSM_POWER_ON	PORTE.OUT|=PIN0_bm
#define GSM_POWER_OFF	PORTE.OUT&=~PIN0_bm

#define GSM_SENSOR_DIR	PORTD.DIR|=PIN1_bm
#define GSM_SENSOR_ON	PORTD.OUT|=PIN1_bm
#define GSM_SENSOR_OFF	PORTD.OUT&=~PIN1_bm

#define GSM_IS_ON		PORTE.IN&PIN1_bm
#define GSM_IS_OFF		!(PORTE.IN&PIN1_bm)
#define GSM_PULL_ON_OFF_DOWN	PORTE.PIN1CTRL|=0x10

#define PORT_CH1 PORTB
#define PORT_CH2 PORTE
#define PORT_BUT PORTA

#define CH1_INT PORTB_INT0_vect
#define CH2_INT PORTE_INT0_vect
#define BUT_INT PORTA_INT0_vect

#define CHARGE_DIR PORTR.DIR|=PIN0_bm
#define CHARGE_ON PORTR.OUT|=PIN0_bm
#define CHARGE_OFF PORTR.OUT&=~PIN0_bm


#define VOLTAGE_BAT ADCCH_POS_PIN3
#define VOLTAGE_INTERNAL ADCCH_POS_PIN0
#define ZERO_ADC ADCCH_POS_PIN1
#define PRESS_SENSOR_1 ADCCH_POS_PIN4
#define PRESS_SENSOR_2 ADCCH_POS_PIN11
#define TEMPERATURE ADCCH_POS_TEMPSENSE

 