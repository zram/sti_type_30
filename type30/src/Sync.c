#include "asf.h"
#include <avr/eeprom.h>
#include "string.h"
#include "main.h"
#include "Sync.h"
#include "Mbus.h"

void verif_imp_start(uint8_t ch);
void verif_imp_stop (uint8_t ch);
void	verif_cur_start(uint8_t ch);
void	verif_cur_stop(uint8_t ch);
void	calibration_cur(uint8_t ch,uint8_t ch_val);
void	calibration_reset(uint8_t ch);

static bool my_flag_autorize_generic_events = false;
volatile uint8_t from_pc[UDI_HID_REPORT_IN_SIZE];
 

volatile uint8_t f_sync;
uint16_t const calib_ch1_4_default=400, calib_ch1_8_default=800,calib_ch1_12_default=1200,calib_ch1_16_default=1600,calib_ch1_20_default=2000;

uint16_t const calib_ch2_4_default=400, calib_ch2_8_default=800,calib_ch2_12_default=1200,calib_ch2_16_default=1600,calib_ch2_20_default=2000;

uint16_t calib_ch1_4,calib_ch1_8,calib_ch1_12,calib_ch1_16,calib_ch1_20;
uint16_t calib_ch2_4,calib_ch2_8,calib_ch2_12,calib_ch2_16,calib_ch2_20;

uint16_t EEMEM calib_ch1_4_nvm=400,calib_ch1_8_nvm=800,calib_ch1_12_nvm=1200,calib_ch1_16_nvm=1600,calib_ch1_20_nvm=2000;
uint16_t EEMEM calib_ch2_4_nvm=400,calib_ch2_8_nvm=800,calib_ch2_12_nvm=1200,calib_ch2_16_nvm=1600,calib_ch2_20_nvm=2000;


uint8_t EEMEM error_nvm=0;
uint8_t EEMEM hours_sync_nvm=8;
uint32_t EEMEM sheduler_nvm=0;
uint32_t EEMEM hours_work_nvm=0;
uint16_t EEMEM send_times_nvm=0;
uint8_t EEMEM  charge_count_nvm=0;
uint16_t EEMEM reset_count_nvm=0;
uint32_t EEMEM ch1_nvm=0,ch2_nvm=0;
uint8_t EEMEM charcge_count_nvm;
uint16_t EEMEM  mark1_up_nvm,mark1_down_nvm,mark2_up_nvm,mark2_down_nvm,cycle_min_nvm=4320;
uint8_t EEMEM  f_archive_start_nvm=0,archive_period_nvm=0,archive_size_nvm=0,f_mark_nvm=0,mark_period_nvm=0,sensor_on_nvm=0;

extern struct calendar_date date;

void sync(void)
{
 
	if (from_pc[0]==0xF0) //SEND TO PC!
	{
		if (from_pc[1]==0)
		{
			
			memcpy(from_pc,&t_frame.start,sizeof(t_frame)-161);

			udi_hid_generic_send_report_in(from_pc);
			
		}
	}
	
	//RECIEVE FROM PC
	if (from_pc[0]==0xA0)
	{
		settings_update(from_pc);

	}
	
	
if ((from_pc[0]==0xC1)||(from_pc[0]==0xC2))
{
	volatile uint8_t mbus_sync_timer;
	mbus_sync_timer=sec_timer;
	mbus_transsmit(from_pc+2,from_pc[1]);
	while ((!f_have_mbus)&&(sec_timer<mbus_sync_timer+2)){}
	
	if 	 (f_have_mbus)
	{
		f_have_mbus=0;
		udi_hid_generic_send_report_in(input_mbus);
		if (from_pc[6]==0x68 && from_pc[7]==0x10)
		{
			if ((from_pc[0]==0xC1)&&(from_pc[9]!=0xAA))
			{
				memcpy(&mbus_addr1,&from_pc[20],7);
				eeprom_write_block(&mbus_addr1,&mbus_addr1_nvm,7);
			}
			else if ((from_pc[0]==0xC2)&&(from_pc[9]!=0xAA))
			{
				memcpy(&mbus_addr2,&from_pc[20],7);
				eeprom_write_block(&mbus_addr2,&mbus_addr2_nvm,7);
			}
		}
	}
	else
	udi_hid_generic_send_report_in("ERROR: MBUS IS NOT RESPONDING");
}
	
	
	if (from_pc[0]==0xD2)
	{
		f_debug = from_pc[1];
		udi_hid_generic_send_report_in(from_pc);
		mesure();
	}
	if (from_pc[0]==0xD3)
	{
		d_mesage(d_ID);
	}
	
	if (from_pc[0]==0xD4)
	{
		d_mesage(d_ADC_INT);
	}
	if (from_pc[0]==0xD5)
	{
		d_mesage(d_ADC_EXT);
	}
	if (from_pc[0]==0xD6)
	{
		d_mesage(d_ADC_TEMP);
	}
	
		if (from_pc[0]==0xF5)
		{from_pc[0]=0;verif_imp_start(from_pc[1]);}
		
		if (from_pc[0]==0xF6)
		{from_pc[0]=0;verif_imp_stop(from_pc[1]);}
		
		if (from_pc[0]==0xF7)
		{from_pc[0]=0;verif_cur_start(from_pc[1]);}
		
		if (from_pc[0]==0xF8)
		{from_pc[0]=0;verif_cur_stop(from_pc[1]);}
		
		if (from_pc[0]==0xF9)
		{from_pc[0]=0;calibration_cur(from_pc[1],from_pc[2]);}
		
		if (from_pc[0]==0xFA)
		{from_pc[0]=0;calibration_reset(from_pc[1]);}
}

uint32_t ch1=0,ch2=0;

void verif_imp_start (uint8_t ch)
{
	uint8_t record_mass[3]={0xF5,0,0};
	if (ch==1)
	{
		ch1=t_frame.ch1; record_mass[1]=0x01;
		udi_hid_generic_send_report_in(record_mass);
	}

	if (ch==2)
	{
		ch2=t_frame.ch2; record_mass[1]=0x02;
		udi_hid_generic_send_report_in(record_mass);
	}
}
void verif_imp_stop (uint8_t ch)
{
	uint8_t record_mass[4]; uint8_t mas_n=0;
	if (ch==1)
	{
		ch1=t_frame.ch1-ch1;
		record_mass[mas_n]=0xF6;mas_n++;
		record_mass[mas_n]=ch;mas_n++;
		record_mass[mas_n]=ch1>>24;mas_n++;
		record_mass[mas_n]=ch1>>16;mas_n++;
		record_mass[mas_n]=ch1>>8;mas_n++;
		record_mass[mas_n]=ch1;mas_n++;
		udi_hid_generic_send_report_in(record_mass);
		 
	}
	else if (ch==2)
	{
		ch2=t_frame.ch2-ch2;
		record_mass[mas_n]=0xF6;mas_n++;
		record_mass[mas_n]=ch;mas_n++;
		record_mass[mas_n]=ch2>>24;mas_n++;
		record_mass[mas_n]=ch2>>16;mas_n++;
		record_mass[mas_n]=ch2>>8;mas_n++;
		record_mass[mas_n]=ch2;mas_n++;
		udi_hid_generic_send_report_in(record_mass);
		 
	}
	else
	{
		record_mass[mas_n]=0xF6;mas_n++;
		record_mass[mas_n]=0xF6;mas_n++;
		udi_hid_generic_send_report_in(record_mass);
		 
	}
}
void verif_cur_start(uint8_t ch)
{
	uint8_t record_mass[3];
	record_mass[0]=0xF7;
	record_mass[1]=ch;
	mesure_presure(1);
	udi_hid_generic_send_report_in(record_mass);
	 
}
void verif_cur_stop(uint8_t ch)
{ 
	uint8_t record_mass[5];
	if (ch==1)
	{
		record_mass[0]=0xF8;
		record_mass[1]=ch;
		record_mass[2]=t_frame.press_sensor1>>8;
		record_mass[3]=t_frame.press_sensor1;
	udi_hid_generic_send_report_in(record_mass);}
	else
	if (ch==2)
	{
		record_mass[0]=0xF8;
		record_mass[1]=ch;
		record_mass[2]=t_frame.press_sensor2>>8;
		record_mass[3]=t_frame.press_sensor2;
	udi_hid_generic_send_report_in(record_mass);}
	//else 
}
void calibration_cur(uint8_t ch,uint8_t ch_val)
{
	uint8_t record_mass[4]={0xF9,0,0,0};
	if (ch==1)
	{
		record_mass[1]=1;
		if (ch_val==4)
		{
			record_mass[2]=4;
			mesure_presure(1);
			calib_ch1_4=t_frame.press_sensor1;
			if (calib_ch1_4>200 && calib_ch1_4<600)
			{
				eeprom_write_word(&calib_ch1_4_nvm,calib_ch1_4);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==8)
		{
			record_mass[2]=8;
			mesure_presure(1);
			calib_ch1_8=t_frame.press_sensor1;
			if (calib_ch1_8>600 && calib_ch1_8<1000)
			{
				eeprom_write_word(&calib_ch1_8_nvm,calib_ch1_8);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==12)
		{
			record_mass[2]=12;
			mesure_presure(1);
			calib_ch1_12=t_frame.press_sensor1;
			if (calib_ch1_12>1000 && calib_ch1_12<1400)
			{
				eeprom_write_word(&calib_ch1_12_nvm,calib_ch1_12);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==16)
		{
			record_mass[2]=16;
			mesure_presure(1);
			calib_ch1_16=t_frame.press_sensor1;
			if (calib_ch1_16>1400 && calib_ch1_16<1800)
			{
				eeprom_write_word(&calib_ch1_16_nvm,calib_ch1_16);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==20)
		{
			record_mass[2]=20;
			mesure_presure(1);
			calib_ch1_20=t_frame.press_sensor1;
			if (calib_ch1_20>1800 && calib_ch1_20<2200)
			{
				eeprom_write_word(&calib_ch1_20_nvm,calib_ch1_20);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
	}


	if (ch==2)
	{
		record_mass[1]=2;
		if (ch_val==4)
		{
			record_mass[2]=4;
			mesure_presure(1);
			calib_ch2_4=t_frame.press_sensor2;
			if (calib_ch2_4>200 && calib_ch2_4<600)
			{
				eeprom_write_word(&calib_ch2_4_nvm,calib_ch2_4);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==8)
		{
			record_mass[2]=8;
			mesure_presure(1);
			calib_ch2_8=t_frame.press_sensor2;
			if (calib_ch2_8>600 && calib_ch2_8<1000)
			{
				eeprom_write_word(&calib_ch2_8_nvm,calib_ch2_8);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==12)
		{
			record_mass[2]=12;
			mesure_presure(1);
			calib_ch2_12=t_frame.press_sensor2;
			if (calib_ch2_12>1000 && calib_ch2_12<1400)
			{
				eeprom_write_word(&calib_ch2_12_nvm,calib_ch2_12);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==16)
		{
			record_mass[2]=16;
			mesure_presure(1);
			calib_ch2_16=t_frame.press_sensor2;
			if (calib_ch2_16>1400 && calib_ch2_16<1800)
			{
				eeprom_write_word(&calib_ch2_16_nvm,calib_ch2_16);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
		if (ch_val==20)
		{
			record_mass[2]=20;
			mesure_presure(1);
			calib_ch2_20=t_frame.press_sensor2;
			if (calib_ch2_20>1800 && calib_ch2_20<2200)
			{
				eeprom_write_word(&calib_ch2_20_nvm,calib_ch2_20);
				udi_hid_generic_send_report_in(record_mass);
			}
			else {record_mass[2]=0xFF; udi_hid_generic_send_report_in(record_mass);}
		}
		
	}

}
void calibration_reset(uint8_t ch)
{
	uint8_t record_mass[4]={0xFA,0,0,0};
	if (ch==1)
	{	record_mass[1]=ch;
		calib_ch1_4=0;eeprom_write_word(&calib_ch1_4_nvm,calib_ch1_4);
		calib_ch1_8=0;eeprom_write_word(&calib_ch1_8_nvm,calib_ch1_8);
		calib_ch1_12=0;eeprom_write_word(&calib_ch1_12_nvm,calib_ch1_12);
		calib_ch1_16=0;eeprom_write_word(&calib_ch1_16_nvm,calib_ch1_16);
		calib_ch1_20=0;eeprom_write_word(&calib_ch1_20_nvm,calib_ch1_20);
		udi_hid_generic_send_report_in(record_mass);
	}

	if (ch==2)
	{
		record_mass[1]=ch;
		eeprom_write_word(&calib_ch2_4_nvm,calib_ch2_4);
		eeprom_write_word(&calib_ch2_8_nvm,calib_ch2_8);
		eeprom_write_word(&calib_ch2_12_nvm,calib_ch2_12);
		eeprom_write_word(&calib_ch2_16_nvm,calib_ch2_16);
		eeprom_write_word(&calib_ch2_20_nvm,calib_ch2_20);
		udi_hid_generic_send_report_in(record_mass);
	}
}

uint16_t  settings_update( uint8_t * from_pc)
{
	if (from_pc)
	{
		if ((from_pc[0]=='[')&&(from_pc[1]=='['))
		from_pc=from_pc+2;
		volatile uint16_t i=0; uint8_t registr;
		do
		{
			i++;	registr	=from_pc[i];
			switch (from_pc[i])
			{ 
				case 0x0A:  t_frame.ch1=from_pc[i+1]; t_frame.ch1<<=8; t_frame.ch1+=from_pc[i+2]; t_frame.ch1<<=8; t_frame.ch1+=from_pc[i+3]; t_frame.ch1<<=8; t_frame.ch1+=from_pc[i+4]; i+=4; break;
				case 0x0B:  t_frame.ch2=from_pc[i+1]; t_frame.ch2<<=8; t_frame.ch2+=from_pc[i+2]; t_frame.ch2<<=8; t_frame.ch2+=from_pc[i+3]; t_frame.ch2<<=8; t_frame.ch2+=from_pc[i+4]; i+=4; break;
				case 0x0F:  date.year=from_pc[i+1]<<8; date.year+=from_pc[i+2]; i+=2;  break;
				case 0x10:  date.month=from_pc[i+1]; i+=1;break;
				case 0x11:  date.date=from_pc[i+1]; i+=1;break;
				case 0x12:  date.hour=from_pc[i+1]; i+=1;break;
				case 0x13:	i+=1; break;
				case 0x15:  t_frame.time_sync = from_pc[i+1]; i+=1;break;
				case 0x16:  t_frame.sheduler=from_pc[i+1]; t_frame.sheduler<<=8; t_frame.sheduler+=from_pc[i+2]; t_frame.sheduler<<=8; t_frame.sheduler+=from_pc[i+3]; t_frame.sheduler<<=8; t_frame.sheduler+=from_pc[i+4]; i+=4; break;
				case 0x19:  t_frame.charge_count= from_pc[i+1]; i+=1;break; 
				case 0x24:	t_frame.f_archive_start = from_pc[i+1]; i+=1; break;
				case 0x25:	t_frame.archive_period = from_pc[i+1]; i+=1; break;
				case 0x26:	t_frame.archive_size = from_pc[i+1]; i+=1; if (t_frame.archive_size<MAX_ARCHIVE_SIZE)t_frame.archive_size=MAX_ARCHIVE_SIZE; break;
				case 0x2B:	t_frame.f_mark = from_pc[i+1]; i+=1; break;
				case 0x2C:	t_frame.mark1_up=from_pc[i+1]<<8; t_frame.mark1_up+= from_pc[i+2]; i+=2; break;
				case 0x2D:	t_frame.mark1_down=from_pc[i+1]<<8; t_frame.mark1_down+= from_pc[i+2]; i+=2; break;
				case 0x2E:	t_frame.mark2_up=from_pc[i+1]<<8; t_frame.mark2_up+= from_pc[i+2]; i+=2; break;
				case 0x2F:	t_frame.mark2_down=from_pc[i+1]<<8; t_frame.mark2_down+= from_pc[i+2]; i+=2; break;
				case 0x34:	t_frame.mark_period = from_pc[i+1]; i+=1; break;
				case 0x36:	t_frame.sensor_on = from_pc[i+1]; i+=1; break;
				case 0x3B:	t_frame.cycle_min=from_pc[i+1];t_frame.cycle_min<<=8; t_frame.cycle_min+= from_pc[i+2]; i+=2; break; 
				case 93: registr=0; break;  // ��� ������ ] ����� �� ������� �� ������� ������
				
				
				default: registr=0;
			}
			
		}	while (registr!=0);
		
		

		eeprom_update_dword(&ch1_nvm,t_frame.ch1);
		eeprom_update_dword(&ch2_nvm,t_frame.ch2);
		eeprom_update_byte(&hours_sync_nvm,t_frame.time_sync);
		eeprom_update_dword(&sheduler_nvm,t_frame.sheduler);
		eeprom_update_byte(&charcge_count_nvm, t_frame.charge_count);
		
		eeprom_update_byte(&f_archive_start_nvm, t_frame.f_archive_start);
		eeprom_update_byte(&archive_period_nvm, t_frame.archive_period);
		eeprom_update_byte(&archive_size_nvm, t_frame.archive_size);
		eeprom_update_byte(&f_mark_nvm, t_frame.f_mark);
		eeprom_update_word(&mark1_up_nvm, t_frame.mark1_up);
		eeprom_update_word(&mark1_down_nvm, t_frame.mark1_down);
		eeprom_update_word(&mark2_up_nvm, t_frame.mark2_up);
		eeprom_update_word(&mark2_down_nvm, t_frame.mark2_down);
		eeprom_update_byte(&mark_period_nvm, t_frame.mark_period);
		eeprom_update_byte(&sensor_on_nvm, t_frame.sensor_on);
		eeprom_update_word(&cycle_min_nvm, t_frame.cycle_min);
		
		return i;
	}
	else
	return 0;
}



// void send_archve_error_record(uint8_t record)
// {
// 	if (record<(160/8+1)) {
// 		uint8_t record_mass[10];
// 		ind_err = (record+1)*8;;
// 		record_mass[0]=0xF3;
// 		eeprom_read_block(&record_mass[1],&archive_err_nvm[ind_err],8);
// 		record_mass[9]=0xF3;
// 	udi_hid_generic_send_report_in(record_mass);}
// }
//
// void archive_error_erase(void)
// {
// 	ind_err =0;
// 	eeprom_write_word(&ind_err_nvm,ind_err);
// }


void my_callback_generic_report_out(uint8_t *report)
{
	uint8_t i=0;
	while(i<UDI_HID_REPORT_IN_SIZE)
	{
		from_pc[i]=report[i];
		i++;
	}
	config_timer_sec=120;
	f_sync=1;
}

void my_callback_generic_disable(void)
{
	my_flag_autorize_generic_events = false;
}

bool my_callback_generic_enable(void)
{
	mbus_init(1249); 
	my_flag_autorize_generic_events = true;
	return true;
}

void my_callback_generic_set_feature(uint8_t *report_feature){
	if ((report_feature[0] == 0)|| (report_feature[1] == 1)) {
		// The report feature is correct
	}
}



void d_mesage(uint8_t code)
{
	if(f_debug)
	{
		uint8_t debug_message[150];
		
		switch(code)
		{
			case d_ID: { debug_message[0]=code; for (uint8_t i=0; i<11;i++) debug_message[i+1]=t_frame.id[i];
			udi_hid_generic_send_report_in(debug_message);} break;
			case d_BUTTON: {
			debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_CH1:  { debug_message[0]=code; debug_message[1]=t_frame.ch1;debug_message[2]=t_frame.ch1>>8;debug_message[3]=t_frame.ch1>>16;debug_message[4]=t_frame.ch1>>24;	udi_hid_generic_send_report_in(debug_message);	} break;
			case d_CH2:  { debug_message[0]=code; debug_message[1]=t_frame.ch2;debug_message[2]=t_frame.ch2>>8;debug_message[3]=t_frame.ch2>>16;debug_message[4]=t_frame.ch2>>24;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_ADC_INT:  { debug_message[0]=code;	debug_message[1]= t_frame.internal_volt;		udi_hid_generic_send_report_in(debug_message);		} break;
			case d_ADC_EXT:  { debug_message[0]=code;	debug_message[1]= t_frame.external_volt;		udi_hid_generic_send_report_in(debug_message);		} break;
			case d_ADC_TEMP:  { debug_message[0]=code;	debug_message[1]= t_frame.temp;		udi_hid_generic_send_report_in(debug_message);		} break;
			case d_GSM_POWER_ON:  { debug_message[0]=code;		udi_hid_generic_send_report_in(debug_message);			} break;
			case d_GSM_POWER_OFF:  { debug_message[0]=code;		udi_hid_generic_send_report_in(debug_message);			} break;
			case d_GSM_SIM:  { debug_message[0]=code;			udi_hid_generic_send_report_in(debug_message);		} break;
			case d_GSM_NET:   { debug_message[0]=code;			udi_hid_generic_send_report_in(debug_message);		} break;
			case d_GSM_CSQ:  { debug_message[0]=code;	debug_message[1]= t_frame.csq;		udi_hid_generic_send_report_in(debug_message);		} break;
			case d_GSM_GPRS:  { debug_message[0]=code;			udi_hid_generic_send_report_in(debug_message);		} break;
			case d_GSM_SERVER:  { debug_message[0]=code;		udi_hid_generic_send_report_in(debug_message);			} break;
			case d_GSM_OK:   { debug_message[0]=code;			udi_hid_generic_send_report_in(debug_message);		} break;
			case d_GSM_ERROR:  { debug_message[0]=code;	debug_message[1]=t_frame.last_error;			udi_hid_generic_send_report_in(debug_message);		} break;
			case d_SLEEP: { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_RTC:  { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_ERROR_TURN:  { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_ERROR_SIM:  { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_ERROR_REG:  { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_ERROR_GPRS:  { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			case d_ERROR_SERVER:  { debug_message[0]=code;				udi_hid_generic_send_report_in(debug_message);	} break;
			
		}
	}
}